import React, { useReducer } from 'react';
import CursosContext from './cursosContext';
import CursosReducer from './cursosReducer';

import firebase from '../../firebase';

const CursosState = ({ children }) => {
  const initialState = {
    addCursoOk: false,
    loadingAddCurso: false,
    errorAddCurso: null,

    /** cursos */
    cursos: null,
    loadingGetCursos: false,
  };

  const crearCursoContext = async (curso) => {
    dispatch({
      type: 'LOADING_ADD_CURSO',
    });
    try {
      await firebase.db.collection('cursos').add(curso);
      dispatch({
        type: 'ADD_CURSO_OK',
      });
    } catch (error) {
      console.log(error);
      dispatch({
        type: 'ADD_CURSO_ERROR',
      });
    }
  };

  const listarCursos = async () => {
    const doc = await firebase.db.collection('cursos').get();
    const cursos = [];
    doc.forEach((curso) => cursos.push({ id: curso.id, ...curso.data() }));
    dispatch({
      type: 'LISTADO',
      payload: cursos,
    });
  };

  const [state, dispatch] = useReducer(CursosReducer, initialState);

  const value = {
    addCursoOk: state.addCursoOk,
    loadingAddCurso: state.loadingAddCurso,
    errorAddCurso: state.errorAddCurso,
    cursos: state.cursos,
    crearCursoContext,
    listarCursos,
  };

  return (
    <CursosContext.Provider value={value}>{children}</CursosContext.Provider>
  );
};

export default CursosState;
