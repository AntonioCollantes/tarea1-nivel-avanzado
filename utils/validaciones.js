export const validarCrearCuenta = (valores) => {
  let errores = {};
  validarNombre(valores.nombre, errores);
  validarEmail(valores.email, errores);
  validarPassword(valores.password, errores);

  return errores;
};
export const validarLogin = (valores) => {
  let errores = {};
  validarEmail(valores.email, errores);
  validarPassword(valores.password, errores);
  return errores;
};
export const validarCrearCurso = (valores) => {
  let errores = {};
  validarNombre(valores.nombre, errores);
  validarEmpresa(valores.empresa, errores);
  validarUrl(valores.url, errores);
  validarDescripcion(valores.descripcion, errores);
  return errores;
};

const validarNombre = (nombre, errores) => {
  if (!nombre) {
    errores.nombre = 'El nombre es obligatorio';
  }
};

const validarEmail = (email, errores) => {
  if (!email) {
    errores.email = 'El email es obligatorio';
  } else {
    if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(email)) {
      errores.email = 'El correo no es válido';
    }
  }
};
const validarPassword = (password, errores) => {
  if (!password) {
    errores.password = 'El password es obligatorio';
  } else {
    if (password.length < 6) {
      errores.password = 'La contraseña debe de tener al menos 6 caracteres';
    }
  }
};

const validarEmpresa = (empresa, errores) => {
  if (!empresa) {
    errores.empresa = 'La empresa es obligatoria';
  }
};

const validarUrl = (url, errores) => {
  if (!url) {
    errores.url = 'la url es obligatoria';
  } else {
    if (!/^(ftp|http|https):\/\/[^"]+$/.test(url)) {
      errores.url = 'la url es válida';
    }
  }
};

const validarDescripcion = (descripcion, errores) => {
  if (!descripcion) {
    errores.descripcion = 'La empresa es obligatoria';
  }
};
