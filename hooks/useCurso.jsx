import React, { useState, useContext } from 'react';
import { FirebaseContext } from '../firebase';
import { AuthContext } from '../context/auth';

import Router from 'next/router';

export const useCurso = (id, curso) => {
  const [miCurso, setMiCurso] = useState(curso);
  const [comentario, setComentario] = useState({});
  const [loaderComentario, setLoaderComentario] = useState(false);
  const [mensaje, setMensaje] = useState('');
  const [loaderMeGusta, setLoaderMeGusta] = useState(false);
  const [loaderEliminar, setLoaderEliminar] = useState(false);

  const { firebase } = useContext(FirebaseContext);
  const { state } = useContext(AuthContext);

  const { comentarios, votos, votantes, creador, nombreImagen } = miCurso;

  const obtenerCurso = async () => {
    const cursoQuery = await firebase.db.collection('cursos').doc(id);
    const curso = await cursoQuery.get();
    if (curso.exists) {
      setMiCurso(curso.data());
    }
  };

  const comentarioChange = (e) => {
    setMensaje(e.target.value);
    setComentario({ ...comentario, [e.target.name]: e.target.value });
  };

  const agregarComentario = async (e) => {
    e.preventDefault();
    comentario.usuarioId = state.userInfo.uid;
    comentario.usuarioNombre = state.userInfo.displayName;

    const nuevoComentario = [...comentarios, comentario];

    setLoaderComentario(true);

    try {
      await firebase.db
        .collection('cursos')
        .doc(id)
        .update({
          ...miCurso,
          comentarios: nuevoComentario,
        });
      obtenerCurso();
      setMensaje('');
      setLoaderComentario(false);
    } catch (error) {
      console.log(error);
      setLoaderComentario(false);
    }
  };

  const votarCurso = async () => {
    let votosAcumulados = votos + 1;

    if (votantes.includes(state.userInfo.uid)) return;

    const nuevaListaDeVotantes = [...votantes, state.userInfo.uid];

    setLoaderMeGusta(true);

    try {
      await firebase.db.collection('cursos').doc(id).update({
        votos: votosAcumulados,
        votantes: nuevaListaDeVotantes,
      });
      await obtenerCurso();
      setLoaderMeGusta(false);
    } catch (error) {
      console.log(error);
      setLoaderMeGusta(false);
    }
  };

  const eliminarCurso = async () => {
    if (!state.userInfo) return Router.push('/');
    if (creador.id !== state.userInfo.uid) return Router.push('/');

    setLoaderEliminar(true);

    try {
      await firebase.db.collection('cursos').doc(id).delete();
      await firebase.storage.ref('cursos').child(nombreImagen).delete();
      setLoaderEliminar(false);
      Router.push('/');
    } catch (error) {
      console.log(error);
      setLoaderEliminar(false);
    }
  };

  return [
    miCurso,
    state.userInfo,
    loaderComentario,
    loaderMeGusta,
    loaderEliminar,
    mensaje,
    comentarioChange,
    agregarComentario,
    votarCurso,
    eliminarCurso,
  ];
};
