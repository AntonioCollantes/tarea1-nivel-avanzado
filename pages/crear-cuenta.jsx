import React, { useState, useContext } from 'react';
import Router from 'next/router';
import Layout from '../Layout';

import useValidation from '../hooks/useValidation';
import { validarCrearCuenta } from '../utils/validaciones';
import { AuthContext } from '../context/auth';

const intialState = {
  nombre: '',
  email: '',
  password: '',
};

const crearCuenta = () => {
  const [valores, errores, handleChange, handleSubmit] = useValidation(
    intialState,
    validarCrearCuenta,
    () => crearCuenta()
  );

  const { crear } = useContext(AuthContext);

  const crearCuenta = () => {
    // vamos a ejecutar la creacion de la cuenta en firebase
    console.log('vamos a ejecutar la creacion de la cuenta en firebase');
    const { nombre, email, password } = valores;
    crear(nombre, email, password);
    Router.push('/');
  };

  return (
    <>
      <Layout>
        <h1 className='text-center'>crear cuenta</h1>
        <form
          onSubmit={handleSubmit}
          noValidate={true}
          style={{ maxWidth: '30em', margin: '0px auto' }}
        >
          <div className='form-group'>
            <label htmlFor='exampleInputEmail1'>Nombre</label>
            <input
              type='text'
              className='form-control'
              name='nombre'
              value={valores.nombre}
              onChange={handleChange}
            />
            {errores.nombre && (
              <span style={{ fontSize: '14px', color: 'red' }}>
                {errores.nombre}
              </span>
            )}
          </div>
          <div className='form-group'>
            <label htmlFor='exampleInputEmail1'>Correo</label>
            <input
              type='email'
              className='form-control'
              name='email'
              value={valores.email}
              onChange={handleChange}
            />
            {errores.email && (
              <span style={{ fontSize: '14px', color: 'red' }}>
                {errores.email}
              </span>
            )}
          </div>
          <div className='form-group'>
            <label htmlFor='exampleInputPassword1'>Contraseña</label>
            <input
              type='password'
              className='form-control'
              name='password'
              value={valores.password}
              onChange={handleChange}
            />
            {errores.password && (
              <span style={{ fontSize: '14px', color: 'red' }}>
                {errores.password}
              </span>
            )}
          </div>
          <div className='form-group'>
            <button type='submit' className='btn btn-primary'>
              crear cuenta{' '}
            </button>
          </div>
        </form>
      </Layout>
    </>
  );
};

export default crearCuenta;
