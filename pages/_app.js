import 'bootstrap/dist/css/bootstrap.css';

import AuthProvider from '../context/auth';
import CursosState from '../context/cursos/cursosState';

import firebase, { FirebaseContext } from '../firebase';

function MyApp({ Component, pageProps }) {
  return (
    <FirebaseContext.Provider value={{ firebase }}>
      <AuthProvider>
        <CursosState>
          <Component {...pageProps} />
        </CursosState>
      </AuthProvider>
    </FirebaseContext.Provider>
  );
}

export default MyApp;
