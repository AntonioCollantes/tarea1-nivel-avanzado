import React, { useContext, useState } from 'react';
import Router from 'next/router';
import Layout from '../Layout';

import useValidation from '../hooks/useValidation';
import { validarCrearCurso } from '../utils/validaciones';

import { FirebaseContext } from '../firebase';
import { AuthContext } from '../context/auth';
import CursosContext from '../context/cursos/cursosContext';

const intialState = {
  nombre: '',
  empresa: '',
  imagen: '',
  url: '',
  descripcion: '',
};

const NuevoCurso = () => {
  const [valores, errores, handleChange, handleSubmit] = useValidation(
    intialState,
    validarCrearCurso,
    () => crearCurso()
  );

  const [nombreImagen, setNombreImagen] = useState('');
  const [urlImage, setUrlImage] = useState(null);
  const [loadingUpImage, setLoadingUpImage] = useState(false);

  const { nombre, empresa, url, descripcion } = valores;

  const { firebase } = useContext(FirebaseContext);
  const { state } = useContext(AuthContext);
  const { crearCursoContext } = useContext(CursosContext);

  const handleUploadImage = (e) => {
    console.log('subiendo imagen');
    const image = e.target.files[0];
    const uploadTask = firebase.storage.ref(`/cursos/${image.name}`).put(image);
    setLoadingUpImage(true);

    uploadTask.on(
      'state_changed',
      (snapShot) => {
        console.log('snapShot');
        console.log(snapShot);
      },
      (error) => {
        console.log('error');
        console.log(error);
      },
      (event) => {
        firebase.storage
          .ref('cursos')
          .child(image.name)
          .getDownloadURL()
          .then((firebaseURL) => {
            setLoadingUpImage(false);
            setNombreImagen(image.name);
            setUrlImage(firebaseURL);
          });
      }
    );
  };

  const crearCurso = async () => {
    // vamos a ejecutar la creacion de la cuenta en firebase
    console.log('vamos a ejecutar la creacion del curso en firebase');
    const curso = {
      nombre,
      empresa,
      url,
      descripcion,
      urlImage,
      nombreImagen,
      votos: 0,
      votantes: [],
      comentarios: [],
      creado: Date.now(),
      creador: {
        id: state.userInfo.uid,
        nombre: state.userInfo.displayName,
      },
    };
    await crearCursoContext(curso);
    Router.push('/');
  };
  return (
    <Layout>
      <h1 className='text-center'>crear nuevo curso</h1>
      <form onSubmit={handleSubmit} className='jumbotron' noValidate={true}>
        <div className='form-row'>
          <div className='form-group col-md-6'>
            <label htmlFor='nombre'>Nombre</label>
            <input
              type='text'
              className='form-control'
              name='nombre'
              value={valores.nombre}
              onChange={handleChange}
            />
            {errores.nombre && (
              <span style={{ fontSize: '14px', color: 'red' }}>
                {errores.nombre}
              </span>
            )}
          </div>
          <div className='form-group col-md-6'>
            <label htmlFor='empresa'>Empresa</label>
            <input
              type='text'
              className='form-control'
              name='empresa'
              value={valores.empresa}
              onChange={handleChange}
            />
            {errores.empresa && (
              <span style={{ fontSize: '14px', color: 'red' }}>
                {errores.empresa}
              </span>
            )}
          </div>
        </div>
        <div className='form-row'>
          <div className='form-group col-md-6'>
            <label htmlFor='empresa'>Url</label>
            <input
              type='text'
              className='form-control'
              name='url'
              value={valores.url}
              onChange={handleChange}
            />
            {errores.url && (
              <span style={{ fontSize: '14px', color: 'red' }}>
                {errores.url}
              </span>
            )}
          </div>
          <div className='form-group col-md-6'>
            <label htmlFor='archivo'>Subir Archivo</label> <br />
            <input
              type='file'
              accept='image/*'
              name='imagen'
              onChange={handleUploadImage}
            />
            {loadingUpImage && (
              <div className='spinner-border text-info'>
                <span className='sr-only'>Cargando ...</span>
              </div>
            )}
          </div>
        </div>
        <div className='form-row'>
          <div className='form-group col-md-12'>
            <label htmlFor='exampleFormControlTextarea1'>
              Descripción del curso
            </label>
            <textarea
              className='form-control'
              name='descripcion'
              rows='3'
              value={valores.descripcion}
              onChange={handleChange}
            ></textarea>
            {errores.descripcion && (
              <span style={{ fontSize: '14px', color: 'red' }}>
                {errores.descripcion}
              </span>
            )}
          </div>
        </div>
        <div className='form-group'>
          <button type='submit' className='btn btn-primary'>
            crear curso{' '}
          </button>
        </div>
      </form>
    </Layout>
  );
};

export default NuevoCurso;
