import { useContext, useEffect, useState } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import Layout from '../Layout';
import CardCurso from '../components/CardCurso';

import firebase from '../firebase';

const Buscar = ({ cursos }) => {
  const [resultado, setResultado] = useState([]);
  const router = useRouter();
  const {
    query: { q },
  } = router;

  useEffect(() => {
    if (cursos && cursos.length > 0) {
      if (q) {
        const busqueda = q.toLowerCase();
        const filtro = cursos.filter((curso) => {
          return (
            curso.descripcion.toLowerCase().includes(busqueda) ||
            curso.nombre.toLowerCase().includes(busqueda)
          );
        });
        setResultado(filtro);
      }
    }
  }, [q, cursos]);

  return (
    <>
      <Head>
        <title>Bienvenidos al curso de next</title>
        <meta name='description' content='curso de next' />
        <link rel='icon' href='/favicon.ico' />
      </Head>
      <Layout>
        <div className='row mt-4'>
          {resultado.length > 0 ? (
            resultado.map((curso) => <CardCurso key={curso.id} curso={curso} />)
          ) : (
            <div>Por el momento no tenemos este curso disponible</div>
          )}
        </div>
      </Layout>
    </>
  );
};

export default Buscar;

export async function getStaticProps(context) {
  const doc = await firebase.db.collection('cursos').get();
  const cursos = [];
  doc.forEach((curso) => cursos.push({ id: curso.id, ...curso.data() }));
  return {
    props: { cursos },
  };
}
