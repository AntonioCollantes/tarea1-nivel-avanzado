import React from 'react';
import Layout from '../Layout';
import CardCurso from '../components/CardCurso';
import firebase from '../firebase';
import Head from 'next/head';

function Populares ({cursos}) {
  return (
    <>
      <Head>
        <title>CURSOS POPULARES</title>
        <meta name='description' content='curso de next' />
        <link rel='icon' href='/favicon.ico' />
      </Head>
      <Layout>
        <div className='row mt-4'>
          {cursos &&
            cursos.map((curso) => <CardCurso key={curso.id} curso={curso} />)}
        </div>
      </Layout>
    </>
  );
};

export async function getStaticProps(context) {
  const doc = await firebase.db.collection('cursos').get();
    let cursos = [];
    doc.forEach(
      (curso) => cursos.push({ id: curso.id, ...curso.data() })
    );

    cursos.sort((a, b) => {
        if(a.votos < b.votos) {
          return 1;
        }
        if(a.votos > b.votos) {
          return -1;
        }
        return 0;
    });

    return {
      props: {cursos}
    }
}

export default Populares;
