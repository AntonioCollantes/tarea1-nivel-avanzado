import React, { useContext } from 'react';
import { useRouter } from 'next/router';
import Layout from '../Layout';
import useValidation from '../hooks/useValidation';
import { validarLogin } from '../utils/validaciones';

import { AuthContext } from '../context/auth';

const intialState = {
  email: '',
  password: '',
};

const Login = () => {
  const [valores, errores, handleChange, handleSubmit] = useValidation(
    intialState,
    validarLogin,
    () => iniciarSesion()
  );

  const router = useRouter();

  const { iniciar } = useContext(AuthContext);

  const iniciarSesion = () => {
    console.log('vamos a iniciar sesion');
    const { email, password } = valores;
    iniciar(email, password);
    router.push('/');
  };
  return (
    <Layout>
      <h1 className='text-center'>Iniciar Sesión</h1>
      <form
        onSubmit={handleSubmit}
        noValidate={true}
        style={{ maxWidth: '30em', margin: '0px auto' }}
      >
        <div className='form-group'>
          <label htmlFor='exampleInputEmail1'>Correo</label>
          <input
            type='email'
            className='form-control'
            name='email'
            value={valores.email}
            onChange={handleChange}
          />
          {errores.email && (
            <span style={{ fontSize: '14px', color: 'red' }}>
              {errores.email}
            </span>
          )}
        </div>
        <div className='form-group'>
          <label htmlFor='exampleInputPassword1'>Contraseña</label>
          <input
            type='password'
            className='form-control'
            name='password'
            value={valores.password}
            onChange={handleChange}
          />
          {errores.password && (
            <span style={{ fontSize: '14px', color: 'red' }}>
              {errores.password}
            </span>
          )}
        </div>
        <div className='form-group'>
          <button type='submit' className='btn btn-primary'>
            iniciar{' '}
          </button>
        </div>
      </form>
    </Layout>
  );
};

export default Login;
