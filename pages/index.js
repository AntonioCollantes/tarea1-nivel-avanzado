//import { useContext, useEffect } from 'react';
import Head from 'next/head';
import Layout from '../Layout';
// import CursosContext from '../context/cursos/cursosContext';
import CardCurso from '../components/CardCurso';
// import Image from 'next/image';
// import styles from '../styles/Home.module.css';

import firebase from '../firebase';

export default function Home({cursos}) {
  // const { listarCursos, cursos } = useContext(CursosContext);

  // useEffect(() => {
  //   listarCursos();
  // }, []);
  return (
    <>
      <Head>
        <title>Bienvenidos al curso de next</title>
        <meta name='description' content='curso de next' />
        <link rel='icon' href='/favicon.ico' />
      </Head>
      <Layout>
        <div className='row mt-4'>
          {cursos &&
            cursos.map((curso) => <CardCurso key={curso.id} curso={curso} />)}
        </div>
      </Layout>
    </>
  );
}

export async function getStaticProps(context) {
  const doc = await firebase.db.collection('cursos').get();
    const cursos = [];
    doc.forEach((curso) => cursos.push({ id: curso.id, ...curso.data() }));
    return {
      props: {cursos}
    }
}