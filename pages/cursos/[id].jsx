import React from 'react';
import { useRouter } from 'next/router';
import Layout from '../../Layout';
import firebase from '../../firebase';

import { useCurso } from '../../hooks/useCurso';

const Curso = ({ curso }) => {
  const router = useRouter();
  const {
    query: { id },
  } = router;
  const [
    miCurso,
    usuario,
    loaderComentario,
    loaderMeGusta,
    loaderEliminar,
    mensaje,
    comentarioChange,
    agregarComentario,
    votarCurso,
    eliminarCurso,
  ] = useCurso(id, curso);
  const {
    nombre,
    creado,
    urlImage,
    url,
    votos,
    empresa,
    descripcion,
    comentarios,
    creador,
  } = miCurso;

  const puedoElimiarCurso = () => {
    if (!usuario) return false;
    if (creador.id === usuario.uid) return true;
  };

  return (
    <Layout>
      <section className='row'>
        <div className='col-md-12 mt-4'>
          <h4 className='text-center'>{nombre}</h4>
        </div>
        <div className='col-md-6'>
          <small className='text-muted'>Publicado hace {creado}</small>
          <img src={urlImage} className='img-fluid' alt={nombre} />
          {usuario && (
            <>
              <h4 className='mt-2'>Agregar comentario</h4>
              <form className='row' onSubmit={agregarComentario}>
                <div className='col-9'>
                  <input
                    type='text'
                    className='form-control'
                    name='mensaje'
                    value={mensaje}
                    onChange={comentarioChange}
                  />
                </div>
                <div className='col-3'>
                  <button type='submit' className='btn btn-primary btn-block'>
                    Agregar
                    {loaderComentario && (
                      <span
                        className='spinner-border spinner-border-sm'
                        role='satus'
                        arial-hidden='true'
                      ></span>
                    )}
                  </button>
                </div>
              </form>
            </>
          )}

          <h4 className='mt-3'>Comentarios</h4>
          {comentarios.length === 0 ? (
            <p>Se el primero en dejar tu comentario</p>
          ) : (
            <ul className='list-group'>
              {comentarios.map((comentario, index) => (
                <li key={index}>{comentario.mensaje}</li>
              ))}
            </ul>
          )}
        </div>
        <div className='col-md-6'>
          <div className='row'>
            <div className='col-12'>
              <div className='mt-4'>
                <a
                  href={url}
                  target='_blank'
                  type='button'
                  className='btn btn-info'
                >
                  <i className='fa fa-long-arrow-right' aria-hidden='true'></i>{' '}
                  ir a la página oficial
                </a>
              </div>
              <div className='mt-3'>
                {usuario && (
                  <button
                    type='button'
                    className='btn btn-danger mr-2'
                    onClick={votarCurso}
                  >
                    Me gusta{' '}
                    <i className='fa fa-heart-o' aria-hidden='true'></i>{' '}
                  </button>
                )}

                {loaderMeGusta ? (
                  <span
                    className='spinner-border spinner-border-sm'
                    role='satus'
                    arial-hidden='true'
                  ></span>
                ) : (
                  <span
                    className='badge badge-primary'
                    style={{ fontSize: '20px' }}
                  >
                    {votos} <i className='fa fa-heart-o' aria-hidden='true'></i>{' '}
                  </span>
                )}
              </div>
              <div></div>
            </div>
            <div className='col-12 mt-2'>
              <h4>
                Descripción{' '}
                <small className='text-muted'>(empresa: {empresa})</small>
              </h4>
              <small className='text-muted'>(publicado por : eduardo)</small>
              <p>{descripcion}</p>
            </div>
            {puedoElimiarCurso() && (
              <div className='col-md-12'>
                <button
                  className='btn btn-danger btn-block'
                  onClick={eliminarCurso}
                >
                  Eliminar Producto{' '}
                  {loaderEliminar && (
                    <span
                      className='spinner-border spinner-border-sm'
                      role='satus'
                      arial-hidden='true'
                    ></span>
                  )}
                </button>
              </div>
            )}
          </div>
        </div>
      </section>
    </Layout>
  );
};

export default Curso;

export async function getServerSideProps(context) {
  console.log('ejecutandose .....');
  console.log(context.params);
  const { params } = context;
  const { id } = params;

  const doc = await firebase.db.collection('cursos').doc(id);

  const curso = await doc.get();

  return {
    props: {
      curso: curso.data(),
    },
  };
}
